# f030stndriver

MCU direct driver for the STM32F030F4P6 to the GDC21310 display only currently.

## Hardware requirements

Every COM pin should have an RC pull down network to be able to generate 4 voltage
levels by changing the duty cycle of the PWM.

