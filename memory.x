/* Linker script for the STM32F030F4P6 */
MEMORY
{
  FLASH : ORIGIN = 0x08000000, LENGTH = 32K /* 16K, but lets try if it work */
  RAM : ORIGIN = 0x20000000, LENGTH = 4K
}
_stack_start = ORIGIN(RAM) + LENGTH(RAM);
