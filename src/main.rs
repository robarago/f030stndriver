#![no_main]
#![no_std]

use panic_halt as _;
use stm32f0xx_hal as hal;

use core::{cell::RefCell, ops::DerefMut};
use cortex_m::{
    interrupt::CriticalSection, interrupt::Mutex, peripheral::syst::SystClkSource, Peripherals,
};
use cortex_m_rt::{entry, exception};
use cortex_m_semihosting::hprint;
use hal::{
    gpio::*,
    pac,
    prelude::*,
    pwm,
    pwm::{PwmChannels, C1, C2, C3},
    stm32::{TIM1, TIM3},
};

// SysTick reload counter
const RELOAD_TICKS: u32 = 100_000;

// Every RELOAD_TICKS * 12 ticks, the counter is updated
// Just use then a multiple of 12 here to speed up the count
const COUNT_INC_TICKS: u32 = 120; // i.e. 120 * 100_000 means 12Mhz = 0.25s

// 1/4 MUX, 1/3 BIAS (most common configuration for commercial TN and STN)
const DMUX: usize = 4;
const SEGS: usize = 4;

// 1/4 DUTY
const V3: u16 = 90;
const V2: u16 = 60;
const V1: u16 = 30;
const V0: u16 = 0;

// 1/3 BIAS: Duty cycle configurations in 4 phases / 3 frames (+, -, cleanup)
const SWEEPS: [[u16; 4]; 12] = [
    [V0, V2, V2, V2],
    [V2, V0, V2, V2],
    [V2, V2, V0, V2],
    [V2, V2, V2, V0],
    [V3, V1, V1, V1],
    [V1, V3, V1, V1],
    [V1, V1, V3, V1],
    [V1, V1, V1, V3],
    [V0, V0, V0, V0],
    [V0, V0, V0, V0],
    [V0, V0, V0, V0],
    [V0, V0, V0, V0],
];

// 4 BP & 4 FP in this case
struct Pins {
    // BP (common pins) configuration: must be PWM pins
    com1: PwmChannels<TIM3, C1>,
    com2: PwmChannels<TIM3, C2>,
    com3: PwmChannels<TIM1, C2>,
    com4: PwmChannels<TIM1, C3>,
    seg: [Pin<Output<PushPull>>; SEGS], // FP (segment group)
}

#[derive(Copy, Clone)]
enum UnitSymbol {
    NoSymbol,
    Centigrade,
    Percentage,
}
use UnitSymbol::{Centigrade, NoSymbol, Percentage};

struct Screen {
    digit1: u8,
    digit2: u8,
    unit: UnitSymbol,
}

#[derive(Copy, Clone)]
enum ActivePinsPerPhase {
    Nopin,
    Zero, // pin# 0: digit1; pin# 2: digit2
    One,  // pin# 1: digit1; pin# 3: digit2
    Both, // pins 0,1: digit1; pins 2,3: digit2
}
use ActivePinsPerPhase::{Both, Nopin, One, Zero};

const NSCREEN: usize = 11;

const SEGMENTS: [[ActivePinsPerPhase; 4]; NSCREEN] = [
    [Zero, Both, Zero, Both],     // 0
    [Nopin, One, Nopin, One],     // 1
    [Zero, One, Both, Zero],      // 2
    [Zero, One, One, Both],       // 3
    [Nopin, Both, One, One],      // 4
    [Zero, Zero, One, Both],      // 5
    [Zero, Zero, Both, Both],     // 6
    [Zero, One, Nopin, One],      // 7
    [Zero, Both, Both, Both],     // 8
    [Zero, Both, One, Both],      // 9
    [Nopin, Nopin, Nopin, Nopin], // blank
];

// Mutex protected structures for our shared GPIOs and SCREEN
static GPIO: Mutex<RefCell<Option<Pins>>> = Mutex::new(RefCell::new(None));
static SCREEN: Mutex<RefCell<Option<Screen>>> = Mutex::new(RefCell::new(None));

#[entry]
fn main() -> ! {
    let mut p = pac::Peripherals::take().unwrap();
    let cp = Peripherals::take().unwrap();

    cortex_m::interrupt::free(move |cs| {
        // Setup system clock
        let mut syst = cp.SYST;
        let mut rcc = p.RCC.configure().sysclk(48.mhz()).freeze(&mut p.FLASH);

        // Prepare GPIOA mutex
        let gpioa = p.GPIOA.split(&mut rcc);

        // Prepare PWM pin channels
        let (chan0, chan1) = (
            (
                gpioa.pa6.into_alternate_af1(cs), // on TIM3_CH1
                gpioa.pa7.into_alternate_af1(cs), // on TIM3_CH2
            ),
            (
                gpioa.pa9.into_alternate_af2(cs),  // on TIM1_CH2
                gpioa.pa10.into_alternate_af2(cs), // on TIM1_CH3
            ),
        );

        // Prepare channels 1,2 to use TIM3
        let pwm0 = pwm::tim3(p.TIM3, chan0, &mut rcc, 20u32.khz());
        let (mut ch1, mut ch2) = pwm0;
        ch1.enable();
        ch2.enable();

        // Channels 3, 4: use TIM1
        let pwm1 = pwm::tim1(p.TIM1, chan1, &mut rcc, 20u32.khz());
        let (mut ch3, mut ch4) = pwm1;
        ch3.enable();
        ch4.enable();

        // Transfer GPIO into a shared structure
        *GPIO.borrow(cs).borrow_mut() = Some(Pins {
            com1: ch1, // on TIM3_CH1
            com2: ch2, // on TIM3_CH2
            com3: ch3, // on TIM1_CH2
            com4: ch4, // on TIM1_CH3
            seg: [
                gpioa.pa0.into_push_pull_output(cs).downgrade(),
                gpioa.pa1.into_push_pull_output(cs).downgrade(),
                gpioa.pa2.into_push_pull_output(cs).downgrade(),
                gpioa.pa3.into_push_pull_output(cs).downgrade(),
            ],
        });

        // configures the system timer to trigger a SysTick exception every second
        syst.set_clock_source(SystClkSource::Core);

        // Set period to 1/60s (60Hz): 48MHz / 3 frames / 4 phases / 60Hz
        syst.set_reload(RELOAD_TICKS);

        // Enable the counter and interrupt
        syst.enable_counter();
        syst.enable_interrupt();

        hprint!("Counting...\n");

        *SCREEN.borrow(cs).borrow_mut().deref_mut() = Some(Screen {
            digit1: 0,
            digit2: 0,
            unit: NoSymbol,
        });
    });

    loop {}
}

fn updatedisplay(cs: &CriticalSection, pins: &mut Pins, phase: usize) {
    // Start with all segments disabled
    for i in 0..SEGS {
        pins.seg[i].set_low().ok();
    }

    // Set duty cycle on every common corresponding to the frame sweep
    let max_duty = pins.com1.get_max_duty();
    pins.com1.set_duty(max_duty / 100 * SWEEPS[phase][0]);
    pins.com2.set_duty(max_duty / 100 * SWEEPS[phase][1]);
    pins.com3.set_duty(max_duty / 100 * SWEEPS[phase][2]);
    pins.com4.set_duty(max_duty / 100 * SWEEPS[phase][3]);

    // And only activate segments in the first (even) frame
    if phase < DMUX {
        // Segments are rendered according to the digits to display
        if let Some(ref mut screen) = *SCREEN.borrow(cs).borrow_mut().deref_mut() {
            // Activations for digit1
            let act0 = SEGMENTS[screen.digit1 as usize][phase];
            match act0 {
                Nopin => {}
                Zero => {
                    pins.seg[0].set_high().ok();
                }
                One => {
                    pins.seg[1].set_high().ok();
                }
                Both => {
                    pins.seg[0].set_high().ok();
                    pins.seg[1].set_high().ok();
                }
            }

            // Activations for digit2
            let act1 = SEGMENTS[screen.digit2 as usize][phase];
            match act1 {
                Nopin => {}
                Zero => {
                    pins.seg[2].set_high().ok();
                }
                One => {
                    pins.seg[3].set_high().ok();
                }
                Both => {
                    pins.seg[2].set_high().ok();
                    pins.seg[3].set_high().ok();
                }
            }

            // Activations for symbols
            if phase == 0 {
                if matches!(screen.unit, Centigrade) {
                    pins.seg[1].set_high().ok();
                } else if matches!(screen.unit, Percentage) {
                    pins.seg[3].set_high().ok();
                }
            }
        }
    }
}

#[exception]
fn SysTick() {
    // Ticks counter
    static mut TICKSPERSECOND: u32 = 0;
    // Phase state variable (change 1/MUX times inside a frame, 2 frames)
    static mut PHASE: usize = 0;

    // Reset phase when reaches 10 and update the display also (no latch)
    if *PHASE == 12 {
        *PHASE = 0;
        *TICKSPERSECOND += 12;
    }
    cortex_m::interrupt::free(|cs| {
        // Borrow access to ours GPIO pin from the shared structure
        if let Some(ref mut pins) = *GPIO.borrow(cs).borrow_mut().deref_mut() {
            updatedisplay(cs, pins, *PHASE);
            *PHASE += 1
        }
        // Update digit count in 12 multiples (COUNT_SPEED)
        if *TICKSPERSECOND == COUNT_INC_TICKS {
            *TICKSPERSECOND = 0;

            if let Some(ref mut screen) = *SCREEN.borrow(cs).borrow_mut().deref_mut() {
                screen.digit2 += 1;
                if screen.digit2 == 10 {
                    screen.digit2 = 0;
                    screen.digit1 += 1;
                    if screen.digit1 == 10 {
                        screen.digit1 = 0;
                        screen.unit = match screen.unit {
                            NoSymbol => Centigrade,
                            Centigrade => Percentage,
                            Percentage => NoSymbol,
                        }
                    }
                }
            }
        }
    });
}
